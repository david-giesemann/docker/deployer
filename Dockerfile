FROM node:16-alpine

WORKDIR deployer/

RUN apk add openssh && \
    apk add lftp
